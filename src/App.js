import React,{Component} from 'react';
import './app.css';

/*
? Каждую фукнцию надо биндить чтобы передавать по приложению?
? Стрелочное объявление функции позволяет сразу же биндить и как тогда лучше объявлять функции?
? А это нормально что выходит так много функций в одном файле?
? Чтобы передать что-то для нужного элемента приложения нужно прокинуть через всех дочерних до этого элемента
  (card->list->cardinfo->header), или можно напрямую (card->header)?
? Передавать отдельно каждую функцию/переменную запарно, передавать массивом?
? Чем определяется стоит ли выносить блок в отдельный файл?
? Если сильно раздробить приложение по файлам не будет ли от этого хуже в плане производительности
  легче прочесть 1 средний файл чем подключать 5 маленьких, или нет?
? Для моментального обновления использовал обновление состояния(this.setState({updated:true})),
  но если я правильно понял, то это же можно сделать через команды жизненного цикла реакта,
  что-то вроде componentDidUpdate(...)?
?  ?
? Еще был вопрос относительно функций в map и ивентах, но я забыл мб по коду посмотреть - вспомню, пока оставлю так?


 */
import Welcome from './components/PopUps/Welcome';
import Header from './components/header';
import Content from './components/content';

export default class App extends Component{
	constructor(props){
		super(props);

		this.state = {
			welcome: localStorage.getItem("name")?localStorage.getItem("name"):true,
		}
		 this.getName = this.getName.bind(this);
	}
	getName(name){
		this.setState({welcome:name});
	}
	render(){
		const welcomePopUp = this.state.welcome!==true?'':<Welcome getName={this.getName}/>;
		return(
			<div>
					{welcomePopUp}
				<div className="App">
					<div className="todo-wrapper">
						<Header/>
						<Content name={this.state.welcome}/>
					</div>
				</div>
			</div>
		)
	}
}