import React,{Component} from 'react'

export default class Box extends Component{
	constructor(props){
		super(props);
		this.state = {
			nextTitle: this.props.boardTitle,
			showBox:false,
		}
		this.renameBoard=this.renameBoard.bind(this);
		this.showBox=this.showBox.bind(this);
		this.handleChange = this.handleChange.bind(this);
	}
	showBox(){
		this.props.showBox(this.state.showBox);
	}
		handleChange(e){
		this.setState({nextTitle: e.target.value});
	}
	renameBoard(){
		this.props.getBoardTitle(this.state.nextTitle);
	}
	render(){
		return(
		<div className="rename-wrapper">
			<div className="renameBoard">
				<span >Rename Board</span>
				<span onClick={this.showBox}>X</span>
			</div>
			<hr/>
			<div className="renameName">
			<span>Name</span>
			<input type="text" value={this.state.nextTitle} onChange={this.handleChange}/>
			<button onClick={this.renameBoard}>Rename</button>
			</div>
		</div>
	)
}
}