import React,{Component} from 'react'
import Box from './box';
import './rename.css';

export default class Rename extends Component{
	constructor(props){
		super(props);
	
	this.state = {
			showBox:false,
		}
		this.showBox=this.showBox.bind(this);
		this.getBoardTitle=this.getBoardTitle.bind(this);
	}
	showBox(showBox="true"){
		this.setState({showBox:!this.state.showBox})
	}
	getBoardTitle(boardTitle){
		this.setState({showBox:!this.state.showBox});
		localStorage["BoardName"]=boardTitle;
	}
	render(){
		const boardName = localStorage["BoardName"]?localStorage["BoardName"]:localStorage["BoardName"]="Untitled Board";
		const box = this.state.showBox?<Box getBoardTitle={this.getBoardTitle} boardTitle={boardName} showBox={this.showBox}/>:'';
		return(
			<div className="content-header">
				<span className="boardName" onClick={this.showBox}>{boardName}</span>
				<span> by {this.props.name}</span>
				{box}
			</div>
		)
	}
}