import React, {Component} from 'react';
import CardInfo from './cardInfo'
export default class CardPopUp extends Component{

	constructor(props){
		super(props);

		this.state = {
			name:"",
			showInfo:false,
		}
		this.showInfo=this.showInfo.bind(this);
	}
	showInfo(){
		this.setState({showInfo:!this.state.showInfo})
	}
	render(){
		const info = this.state.showInfo?<CardInfo name={this.props.name} listName={this.props.listName} listId={this.props.listId} info={this.props.task} showInfo={this.showInfo} addDescription={this.props.addDescription} addComent={this.props.addComent} deleteComment={this.props.deleteComment}  changeComment={this.props.changeComment} deleteCard={this.props.deleteCard} changeCard={this.props.changeCard}/>:'';
		return(
			<div>
				<span onClick={this.showInfo} className="task-text" key={this.props.task.id}>
					{this.props.task.text}
			 		{this.props.task.comments?this.props.task.comments.length>0?
			 			<div><img src={require("./comments.png")} alt="comment" width="15px"/>
				 			<span className="comments">
				 				{this.props.task.comments.length}
				 			</span>
				 		</div>:"":''}
			 	</span>
			 	{info}
			 </div>
		)
	}
}