import React,{Component} from 'react'

export default class Description extends Component{
	constructor(props){
		super(props)
		this.state={
			showBox:false,
			focus: false,
			value:this.props.description?this.props.description:''
		}
		this.showBoxFalse=this.showBoxFalse.bind(this)
		this.showBoxTrue=this.showBoxTrue.bind(this)
		this.addDescription=this.addDescription.bind(this)
		this.handleChange=this.handleChange.bind(this)
	}
	showBoxFalse(){
		this.setState({showBox:!this.state.showBox})
	}
	showBoxTrue(){
		this.setState({showBox:true})
	}
	addDescription(){
		if(this.state.value.length>0)this.props.addDescription(this.props.listId,this.props.taskId,this.state.value)
	}
	handleChange(e){
		this.setState({value:e.target.value})
	}
	//УЗНАТЬ: стирание в 0 считается за удаление?
	render(){
		const box = this.state.showBox?
		<div>
			<button onClick={this.addDescription}>Save</button>
			<span onClick={this.showBoxFalse}>X</span>
		</div>:''
		return(
			<div className="cardInfo-Description">
				<span>Description</span>
				<textarea value={this.state.value} onClick={this.showBoxTrue} onChange={this.handleChange}placeholder="Add a more detailed description..."/>
				{box}
			</div>
		)
	}
}