import React,{Component} from 'react'
import './cardInfo.css'
import Header from './header'
import Description from './description'
import Comments from './comments'

export default class cardInfo extends Component{

	constructor(props){
		super(props);
		this.state={}
	}
	showInfo=(e)=>{
		//УЗНАТЬ: требуется фокус по textarea, только тогда срабатывает esc
		if(e.keyCode===27)this.props.showInfo()
	}
	render(){
		return(
			<div className="PopUp-wrapper" onKeyUp={this.showInfo}>
				<div className="cardInfo-popUp">
					<Header name={this.props.name} listId={this.props.listId} taskId={this.props.info.id} text={this.props.info.text} listName={this.props.listName} showInfo={this.props.showInfo} deleteCard={this.props.deleteCard} changeCard={this.props.changeCard}/>
					<Description addDescription={this.props.addDescription} listId={this.props.listId} taskId={this.props.info.id} description={this.props.info.description}/>
					<Comments listId={this.props.listId} taskId={this.props.info.id} comments={this.props.info.comments} name={this.props.name} addComent={this.props.addComent} deleteComment={this.props.deleteComment}  changeComment={this.props.changeComment}/>		
				</div>
			</div>
		)
	}
}