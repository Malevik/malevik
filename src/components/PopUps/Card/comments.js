import React,{Component} from 'react'
import Comment from './comment'

export default class Comments extends Component{
	constructor(props){
		super(props)
		this.state={
			value:'',
			text:'',
			checked:true
		}
		this.addComent=this.addComent.bind(this)
		this.handleChange=this.handleChange.bind(this)
		this.changeComment=this.changeComment.bind(this)
		this.setState=this.setState.bind(this)
	}
	addComent(){
		if(this.state.value.length>0)this.props.addComent(this.props.listId,this.props.taskId,this.state.value)
		this.setState({value:''})
	}
	handleChange(e){
		this.setState({value:e.target.value})
	}
	changeComment(e){
		this.setState({text:e.target.value})
	}
	setValue(text){
		if(this.state.checked){
		this.setState({text:text})
		return text}
	}
	render(){
		const comments=this.props.comments?this.props.comments.map(comment=>
				<div key={comment.id} className="cardInfo-Comments-single">
					<Comment name={this.props.name} text={comment.text} id={comment.id} changeComment={this.props.changeComment} listId={this.props.listId} taskId={this.props.taskId} deleteComment={this.props.deleteComment}/>
				</div>):''
		return(
			<div className="cardInfo-Comments-wrapper">
				<div className="cardInfo-Comments-addComents-wrapper">
					<span>Add Comment</span>
					<textarea onChange={this.handleChange} placeholder="Write a comment..." value={this.state.value}/>
					<button onClick={this.addComent}>Save</button>
				</div>
				<div className="cardInfo-Comments-all">
					{comments}
				</div>
			</div>
		)
	}
}