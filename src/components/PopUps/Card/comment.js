import React,{Component} from 'react'

export default class Comment extends Component{
	constructor(props){
		super(props)
		this.state={
			value:'',
			text:this.props.text,
			checked:true
		}
		this.handleChange=this.handleChange.bind(this)
		this.deleteComment=this.deleteComment.bind(this)

	
	}
	deleteComment(commentId){
		this.props.deleteComment(this.props.listId,this.props.taskId,commentId)
	}
	handleChange(e){
		this.setState({text:e.target.value},()=>{
			this.props.changeComment(this.props.listId,this.props.taskId,this.props.id,this.state.text)
			})
	}
		render(){
		 const	name=this.props.name;
			return(
				<div>
					<div>
						<span className="comment-author">{name}</span>
						<textarea className="comment-text" onChange={this.handleChange} value={this.state.text}/>
					</div>
					<button onClick={()=>this.deleteComment(this.props.id)} className="btn-delete">Delete</button>
				</div>
				)
		}
	}