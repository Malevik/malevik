import React,{Component} from 'react'

export default class Header extends Component{
	constructor(props){
		super(props)
		this.state={
			value:this.props.text,
			showbox:false
		}
		this.changeHandler=this.changeHandler.bind(this)
		this.deleteCard=this.deleteCard.bind(this)
		this.showbox=this.showbox.bind(this)
	}
	showbox(){
		this.setState({showbox:true})
	}
	changeHandler(e){
		this.setState({value:e.target.value},()=>{
			this.props.changeCard(this.props.listId,this.props.taskId,this.state.value)
			})
	}
	deleteCard(){
		this.props.deleteCard(this.props.listId,this.props.taskId)
	}
	render(){
		return(
			<div className="cardInfo-header">
				<div>
					<div>
						<textarea onClick={this.showbox} onChange={this.changeHandler} value={this.state.value}/>
						<span>in list <b>{this.props.listName}</b> by <b>{this.props.name}</b></span>
					</div>
					<button onClick={this.deleteCard} className="btn-delete btn-delete-card">Delete card</button>
				</div>
				<div onClick={this.props.showInfo}>
					<span>X</span>
				</div>
			</div>
		)
	}
}