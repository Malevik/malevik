import React,{Component} from 'react';
import './welcome.css';

export default class Welcome extends Component{
	constructor(props){
		super(props);

		this.state = {
			name:"",
		}
		this.nameChange = this.nameChange.bind(this);
		this.displayChange = this.displayChange.bind(this);
	}
		nameChange(e){
		this.setState({name: e.target.value});
	}
	displayChange(name){
		if(name.length>0)
		{
			this.props.getName(name);
			localStorage["name"]=name;
		}
	}
	render(){
		return(
			<div className="PopUp-container">
				<div className="PopUp">
					<h1>Введите свое имя</h1>
					<input className="welcomeInput" type="text" value={this.state.name} onChange={this.nameChange} />
					<button className="welcomeBtn" onClick={()=>this.displayChange(this.state.name)}>Подтвердить</button>
				</div>
			</div>
		)
	}
}