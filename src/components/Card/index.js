import React,{Component} from 'react';
import List from './list'
import ListTitle from './listTitle'
import './card.css'
import Base from "../../base";

export default class Card extends Component{
	constructor(props){
		super(props)
		this.state={
			value:'',
			boardId:'0',
			taskText:'',
			checked: true,
			taskToAdd: false,
			someNew: '',
		}
		this.handleChange=this.handleChange.bind(this);
		this.addTask=this.addTask.bind(this);
		this.addDescription=this.addDescription.bind(this);
		this.addComent=this.addComent.bind(this);
		this.deleteComment=this.deleteComment.bind(this);
		this.deleteCard=this.deleteCard.bind(this);
		this.changeCard=this.changeCard.bind(this);
		this.changeComment=this.changeComment.bind(this);
	}
		handleChange(id,title){
			this.setState({newListName:title},()=>{
				const temporaireBase = JSON.parse(localStorage.getItem("base"));
				temporaireBase[id-1].title=this.state.newListName;
				localStorage.setItem("base", JSON.stringify(temporaireBase));
			})
		}
		addTask(listId,text){
			this.setState({updated: true});
			const temporaireBase = JSON.parse(localStorage.getItem("base"));
			if(temporaireBase[listId-1].todos[0]){
				const toDoId=+temporaireBase[listId-1].todos[temporaireBase[listId-1].todos.length-1].id + 1
				temporaireBase[listId-1].todos.push({"id":toDoId+"","text":text});
			}else{
				temporaireBase[listId-1].todos.push({"id":1+"","text":text});
			}
			localStorage.setItem("base",JSON.stringify(temporaireBase));
		}
		addDescription(listId,taskId,text){
			const temporaireBase = JSON.parse(localStorage.getItem("base"));
			temporaireBase[listId-1].todos[taskId-1].description=text
			localStorage.setItem("base",JSON.stringify(temporaireBase));
			this.setState({updated:true})
		}
		addComent(listId,taskId,text){
			const temporaireBase = JSON.parse(localStorage.getItem("base"));
			if(temporaireBase[listId-1].todos[taskId-1].comments&&temporaireBase[listId-1].todos[taskId-1].comments[0]){
			const lastId = temporaireBase[listId-1].todos[taskId-1].comments.length-1
			  let nextId = +temporaireBase[listId-1].todos[taskId-1].comments[lastId].id+1
			 temporaireBase[listId-1].todos[taskId-1].comments.push({"id":nextId+"","text":text})
			}else {
				temporaireBase[listId-1].todos[taskId-1].comments=[{"id":"1","text":text}]
		}			
			localStorage.setItem("base",JSON.stringify(temporaireBase));
			this.setState({updated:true});
		}
		deleteComment(listId,taskId,commentId){
			const temporaireBase = JSON.parse(localStorage.getItem("base"));		
			 for(let i=0;i<temporaireBase[listId-1].todos[taskId-1].comments.length;i++){
			 	if(temporaireBase[listId-1].todos[taskId-1].comments[i].id.indexOf(commentId)===0){
			 		temporaireBase[listId-1].todos[taskId-1].comments.splice(i,1)
			 	 }
			 }
			 localStorage.setItem("base",JSON.stringify(temporaireBase));
			this.setState({updated:true});
		}
		deleteCard(listId,taskId){
			const temporaireBase = JSON.parse(localStorage.getItem("base"));	
			 for(let i=0;i<temporaireBase[listId-1].todos.length;i++){
			 	if(temporaireBase[listId-1].todos[i].id.indexOf(taskId)===0){
			 	 temporaireBase[listId-1].todos.splice(i,1)
			 	 }
			 }
			 localStorage.setItem("base",JSON.stringify(temporaireBase));
			this.setState({updated:true});
		}
		changeCard(listId,taskId,taskText){
			const temporaireBase = JSON.parse(localStorage.getItem("base"));
			for(let i=0;i<temporaireBase[listId-1].todos.length;i++){
				if(temporaireBase[listId-1].todos[i].id.indexOf(taskId)===0){
				temporaireBase[listId-1].todos[i].text=taskText;
			}}	
			localStorage.setItem("base",JSON.stringify(temporaireBase));
			this.setState({updated:true});
		}
		changeComment(listId,taskId,commentId,commentText){
			const temporaireBase = JSON.parse(localStorage.getItem("base"));
    	for(let i=0;i<temporaireBase[listId-1].todos[taskId-1].comments.length;i++){
    		if(temporaireBase[listId-1].todos[taskId-1].comments[i].id.indexOf(commentId)===0){
					temporaireBase[listId-1].todos[taskId-1].comments[i].text=commentText;
			}}
			localStorage.setItem("base",JSON.stringify(temporaireBase));
			this.setState({updated:true});
		}
	render(){		
		let base;
		if(localStorage.getItem("base"))base=JSON.parse(localStorage.getItem("base"));else{base=Base;localStorage.setItem("base",JSON.stringify(Base)); };	

		 const soloBoard=base.map(list=>
			<div key={list.id}  className="list">
				<ListTitle title={list.title} handleChange={this.handleChange} id={list.id}/>
				<List name={this.props.name} listBase = {list} listName={this.state.newListName?this.state.newListName:list.title} addTask={this.addTask} addDescription={this.addDescription} addComent={this.addComent} deleteComment={this.deleteComment} changeComment={this.changeComment} deleteCard={this.deleteCard} changeCard={this.changeCard}/>
			</div>)		
		return( 
			<div className="board">
				{soloBoard}
			</div>
		)
}
}