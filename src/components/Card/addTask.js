import React, {Component} from 'react';

export default class addTask extends Component{
	constructor(props){
		super(props);

		this.state = {
			clicked: false,
			value:''
		}
		this.handleChange=this.handleChange.bind(this);
		this.addTask=this.addTask.bind(this);
		this.showBox=this.showBox.bind(this);
	}
	showBox(){
		this.setState({clicked:!this.state.clicked, value:''});
	}
	handleChange(e){
		this.setState({value: e.target.value});
	}
	addTask(){
		this.props.addTask(this.props.base.id,this.state.value);
		this.setState({value:''});

	}
	render(){
		const box=this.state.clicked?
		<div className="addTask-box">
			<textarea value={this.state.value} onChange={this.handleChange}/>
			<div>
				<button onClick={this.addTask}>Add Card</button>
				<span onClick={this.showBox}>X</span>
			</div>
		</div>
		:<span onClick={this.showBox}>+ Add a card</span>;
		return(
			<div>{box}</div>
		)
	}
}