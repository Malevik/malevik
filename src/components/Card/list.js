import React, {Component} from 'react';
import AddTask from './addTask';
import CardPopUp from '../PopUps/Card';
export default class List extends Component{
	constructor(props){
		super(props);
		this.state={
			showCardInfo:true,
		}
	}
	render(){
	  const listName = this.props.listName;
	  const name = this.props.name;
		const tasks = this.props.listBase.todos.map(task=>
				<div className="task"  key={task.id}>
					<CardPopUp name={name} listName={listName} listId={this.props.listBase.id} task={task} addDescription={this.props.addDescription} addComent={this.props.addComent} deleteComment={this.props.deleteComment}  changeComment={this.props.changeComment} deleteCard={this.props.deleteCard} changeCard={this.props.changeCard}/>
				</div>)

		return(
			<div className="task-list">
			<div>{tasks}</div>
				<div className="addTask">
					<AddTask base={this.props.listBase} addTask={this.props.addTask}/>
				</div>
			</div>
		)
	}
}