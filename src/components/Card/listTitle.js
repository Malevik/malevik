import React, {Component} from 'react';
export default class ListTitle extends Component{
	constructor(props){
		super(props);
		this.state={
			title:this.props.title,
		}
		this.handleChange=this.handleChange.bind(this)
	}
	handleChange(e){
		this.setState({title: e.target.value},()=>{this.props.handleChange(this.props.id,this.state.title)})
	}
	render(){
		return(
	  	<textarea onChange={this.handleChange}className="listName" value={this.state.title}/>
		)
	}
}