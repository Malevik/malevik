import React,{Component} from 'react';
import Rename from './PopUps/Rename';
import Card from './Card';
import './content.css';

export default class Content extends Component{
	constructor(props){
		super(props);
		this.state={};
		this.clearBoard=this.clearBoard.bind(this);
	}
	clearBoard(){
		localStorage.clear()
		this.setState({updated:true})
	}
	render(){
		return(
			<div className="content-header">
				<Rename name={this.props.name}/>	<button onClick={this.clearBoard}>Reset board</button>
				<Card name={this.props.name}/>
			</div>
		)
	}
}